from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .forms import Schedule_Form
from .models import Schedule

# Create your views here.
response = {'author': "Shania Nabilah"}

def home(request):
	return render(request, 'home.html')

def about(request):
	return render(request, 'about.html')

def education(request):
	return render(request, 'education.html')

def experience(request):
	return render(request, 'experience.html')

def contact(request):
	return render(request, 'contact.html')

def location(request):
	return render(request, 'location.html')

def form(request):
	return render(request, 'form.html')


def index(request):
    response['schedule_post'] = Schedule_Form
    return render(request, 'schedule.html', response)

def schedule_post(request):
    form = Schedule_Form(request.POST or None)
    if(request.method == 'POST'):
        response['name'] = request.POST['name']
        response['date'] = request.POST['date']
        response['place'] = request.POST['place']
        response['category'] = request.POST['category']
        schedule = Schedule(name=response['name'], date=response['date'], place=response['place'], category=response['category'])
        schedule.save()
        return HttpResponseRedirect('/story4/result')
    else:        
        return HttpResponseRedirect('/')

def schedule_table(request):
    table = Schedule.objects.all()
    response['table'] = table
    return render(request, 'result.html', response)

def reset(request):
    delete = Schedule.objects.all().delete()
    return HttpResponseRedirect('/story4/result')
