from django.db import models
from django.utils import timezone

# Create your models here.
class Schedule(models.Model):
	name = models.CharField(max_length=30)
	date = models.DateTimeField()
	place = models.CharField(max_length=30)
	category = models.CharField(max_length=30)
		