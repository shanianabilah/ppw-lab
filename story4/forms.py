from django import forms

class Schedule_Form(forms.Form):
    error_messages = {
        'required': 'This field is required',
    }
    attrs = {
        'class': 'form-control'
    }
    
    name = forms.CharField(label='Activity name', required=True, max_length=30, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    date = forms.DateTimeField(label='Date', required=True,  widget=forms.DateInput(attrs={'type':'datetime-local', 'class':'form-control'}))
    place = forms.CharField(label='Place', required=True, max_length=30, empty_value='Please fill the place!', widget=forms.TextInput(attrs=attrs))
    category = forms.CharField(label='Category', required=True, max_length=30, empty_value='Please fill the category!', widget=forms.TextInput(attrs=attrs))

