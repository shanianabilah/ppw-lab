from django.conf.urls import url
from .views import home
from .views import about
from .views import education
from .views import experience
from .views import contact
from .views import location
from .views import form
from .views import index
from .views import schedule_post, schedule_table, reset

urlpatterns = [
    url(r'^home', home, name='home'),
    url(r'^about', about, name='about'),
    url(r'^education', education, name='education'),
    url(r'^experience', experience, name='experience'),
    url(r'^contact', contact, name='contact'),
    url(r'^location', location, name='location'),
    url(r'^form', form, name='form'),
    url(r'^Schedule_post', schedule_post, name='Schedule_post'),
    url(r'^schedule', index, name='schedule'),
    url(r'^result', schedule_table, name='result'),
    url(r'^reset', reset, name='reset')
]
